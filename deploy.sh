git fetch --all
git reset --hard origin/master

cd /
rm -rf var/www/html/pozzy/public
rm -rf var/www/html/pozzy/src
rm -rf var/www/html/pozzy/node_modules
rm var/www/html/pozzy/package-lock.json
rm var/www/html/pozzy/package.json
rm var/www/html/pozzy/vue.config.js
rm var/www/html/pozzy/.gitignore
rm var/www/html/pozzy/babel.config.js
rm var/www/html/pozzy/yarn.lock
sudo chmod +x var/www/html/pozzy/deploy.sh